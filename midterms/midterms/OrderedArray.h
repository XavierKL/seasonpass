#pragma once
#include <assert.h>
#include <iostream>
#include <string>

using namespace std;

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value) // learned from https://www.includehelp.com/cpp-programs/sort-an-array-in-ascending-order.aspx
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		//your code goes after this line

		for (int i = 0; i < mMaxSize; i++) {

			for (int n = i + 1; j < mMaxSize; j++) {

				if (mArray[j] < mArray[i] && mArray[j] != NULL) {

					int temporary;
					temporary = mArray[i];
					mArray[i] = mArray[j];

				}
			}
		}
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val) // sourced from https://www.youtube.com/watch?v=vohuRrwbTT4
	{
		//your code goes after this line
		int low = 0;
		int mid;
		int high = mMaxSize - 1;

		while (low <= high) {

			mid = (low + high) / 2;

			if (val == mArray[mid]) {

				return mid;
			}

			else if (val > mArray[mid]) {

				low = mid + 1;
			}

			else {

				high = mid - 1;
			}
		}
		return -1;
	}

private:
	T* mArray; // constructor for the array
	int mGrowSize; // when you input something, what the size is gonna grow into, setting the size of the array
	int mMaxSize; // size of the array
	int mNumElements;  // nubmer of elements in the array

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};