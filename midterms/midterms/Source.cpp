#include <iostream>
#include <string>
#include "UnordredArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main() {

	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";
	cout << " " << endl;

	cout << "Please select an action to do: " << endl
		<< "[1] Remove a number" << endl
		<< "[2] Search for a number" << endl
		<< "[3] Expand and randomize" << endl;
	int choice;
	cin >> choice;

	if (choice == 2) { 
	
	cout << "\nEnter number to search: ";
	int input;
	cin >> input;
	cout << endl;

	cout << "Unordered Array(Linear Search):\n";
	int result = unordered.linearSearch(input);
	if (result >= 0)
		cout << input << " was found at index " << result << ".\n";
	else
		cout << input << " not found." << endl;

	cout << "Ordered Array(Binary Search):\n";
	result = ordered.binarySearch(input);
	if (result >= 0)
	cout << input << " was found at index " << result << ".\n";
	else
		cout << input << " not found." << endl;
	system("pause");
	}

	else if (choice == 1) {

		cout << "Please enter a number to remove" << endl;
		int removeNumber;
		cin >> removeNumber;

		unordered.pop();
		for (int i = 0; i < unordered.getSize(); i++) {

			cout << unordered[i] << "  " << endl;
		}

		ordered.pop();
		for (int i = 0; i < ordered.getSize(); i++) {

			cout << ordered[i] << "  " << endl;
		}
	}

	else if (choice == 3) {
		
		cout << "Please input the number of elements you want to add to the current array" << endl;
		int elementAdder;
		cin >> elementAdder;

		unordered.push(elementAdder);
		for (int i = 0; i < elementAdder; i++) {

			cout << unordered[elementAdder] << "   " << endl;
		}
		ordered.push(elementAdder);
		for (int i = 0; i < elementAdder; i++) {

			cout << ordered[elementAdder] << "   " << endl;
		}
	}
}