#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"

using namespace std; 

int main() {


	int sizeInput;
	int choice = 0;


	cout << "enter the size of the array" << endl;
	cin >> sizeInput;

	stack<int> stack(sizeInput);
	queue<int> queue(sizeInput);

	system("cls");

	while (choice != 4) {

		cout << "choose what you want to do" << endl;
		cout << "[1] Push an element" << endl
		<< "[2] Pop the top of the array" << endl
		<< "[3] Print all elements of the array and erase" << endl
		<< "[4] End program" << endl;
		
 		cin >> choice;

		system("cls");

		if (choice == 1) {

			int element;

			cout << "input number to push: ";
			cin >> element;

			stack.push(element);
			queue.push(element);

			
			cout << "top element of the sets: " << endl;
			cout << "stack: " << stack.top() << endl; // displays wrong element 
			cout << "queue: " << queue.top() << endl;
			

			system("pause");
			system("cls");

			
		}	

		else if (choice == 2) {

			cout << "popped the top part of the set" << endl;
			stack.pop(); // pops the first one in the stack 
			queue.remove(); // must modify to pop the first one 
			system("pause");
			system("cls");
		}

		else if (choice == 3) {

			cout << "here are the sets: " << endl
				<< "Stack" << endl;
			for (int n = 0; n < stack.getSize(); n++) {

				cout << stack[n] << endl;
			}

			cout << "Queue" << endl;
			for (int i = 0; i < queue.getSize(); i++) {

				cout << queue[i] << endl;
			}

			for (int g = 0; g < sizeInput; g++) {

				stack.pop();

			}

			for (int r = 0; r < sizeInput; r++) {

				queue.pop();
			}

			system("pause");
			system("cls");

		}

		else if (choice == 4) {

			break;
		}
	}

	return 0;
}

