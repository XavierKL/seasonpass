#pragma once
#include <assert.h>

template<class T>
class queue
{
public:
	queue(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~queue()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value) //modify 
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual void pop() // modify
	{
		if (mNumElements < 0) {

			
		}
			
	}

	virtual void remove()
	{

		for (int i = 0; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}

	virtual int linearSearch(T val)
	{
		for (int i = 0; i < mNumElements; i++)
			if (mArray[i] == val)
				return i;

		return -1;
	}

	const T& top() {

		return mArray[0];
	}
private:

	T* mArray; // the array itself
	int mGrowSize; 
	int mMaxSize;
	int mNumElements; // elements of the array

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};