#pragma once
#include <assert.h>

template<class T>
class UnorderedArray {

public:

	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0) {

		if (size) {

			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);

			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~UnorderedArray() {

		if (mArray != NULL) {

			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value) {

		assert(mArray != NULL);

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual T& operator[](int index) {

		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];

	}

	int getsize() {

		return mNumElements;
	}

	virtual void pop() {

		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index) {

		assert(mArray != NULL);

		if (index >= mMaxSize)
			return;

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

private:

	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

};
